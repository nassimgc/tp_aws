// We import the aws sdk
const AWS = require("aws-sdk");
//Thanks to the aws sdk we call the DynamoDb Document Client
const dynamo = new AWS.DynamoDB.DocumentClient();
// We store the name of our table in constant
const dynamoTable = "student";

//We import the buildResponse function
const responseFunction = require("./response.js");
/*The function that will show the data of a student. 
We need to give the id of the student which send in parameter*/
async function searchStudent(requestBody) {
    
    
    /* We call the dynamicSearchQuery to prepare the search
    expressions needed for the parameters. We will give
    the requestBody as parameter of the function*/
    let expression = dynamicSearchQuery(requestBody);
    // Then we give the parameters and inside we describe : 
    const params = {
        // The name of table
        TableName: dynamoTable,
        
        /* We use the spread operator to spread the object that 
           we retrieve from the dynamicUpdateQuery function*/
           
        ...expression
    }
    /*Then we return a promise where the documentClient call the method scan 
    to show the data of the student with parameters that we describe before*/
    return await dynamo.scan(params).promise().then((response)=>{
        //We return buildResponse with the student data and the code 200
        return responseFunction.buildResponse(200,response.Items);
    }, (error) => {
        // if there is an error, we send a code 404 and the description of the error
        return responseFunction.buildResponse(404,error);
    });
}



/*function that will add dynamically the value inside the filter expression
according to the JSON body request in parameters*/

function dynamicSearchQuery(requestBody)
{
    /*
        We define here an object which contain all expressions
        that we need to make our search in a dynamic way
    */
    let expressionObject = {
        FilterExpression: '',
        ExpressionAttributeNames: {},
        ExpressionAttributeValues: {}
    }
    
    /*
        We iterate the requestBody to fill the expressions
        inside of the object
    */
    Object.entries(requestBody).forEach(([key, item]) => {
        /*We fill the filter expression. We need it to use the scan method
        inside the searchStudent function. 
        eg : FilterExpression : #nom = :nom AND #prenom = :prenom
        */
        expressionObject.FilterExpression += ` #${key} = :${key} AND`;
        //We declare the attributes names that we use in our filter expression
        expressionObject.ExpressionAttributeNames[`#${key}`] = key;
        //We declare the attributes values that we use in our filter expression
        expressionObject.ExpressionAttributeValues[`:${key}`] = item;
    });
    
    /*  We use here a slice on the filter expression
        to remove the last "AND" to avoid an error */
    expressionObject.FilterExpression = expressionObject.FilterExpression.slice(0, -3);
    
    //We return the object
    return expressionObject;
    
}

// We export here the function to call it in the index.js
module.exports = { searchStudent };