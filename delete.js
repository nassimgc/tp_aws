// We import the aws sdk
const AWS = require("aws-sdk");
//Thanks to the aws sdk we call the DynamoDb Document Client
const dynamo = new AWS.DynamoDB.DocumentClient();
// We store the name of our table in constant
const dynamoTable = "student";

//We import the buildResponse function
const responseFunction = require("./response.js");

/*The function that will delete the student. 
We need to give the id of the student which send in parameter*/
async function deleteStudent(studentId) {
    // Then we give the parameters and inside we describe : 
    const params = {
        TableName: dynamoTable,
        Key: {
            "id": studentId,
        },
        //We check if the id exist or not in the table
        ConditionExpression: "attribute_exists(id)"
    }
    /*Then we return a promise where the documentClient call the method delete 
    to delete the data of the student with parameters that we describe before*/
    return await dynamo.delete(params).promise().then((response)=>{
         // We build the body of the message of success if the put method going well
        const body = {
            Operation: 'DELETE',
            Message:'SUCCESS'
        }
        //We return buildResponse with the message of success and the code 200
        return responseFunction.buildResponse(200,body);
        
    }, (error) => {
        // We check if there is an error
        if(error.message == "The conditional request failed")
        {
            /* If the error have the message "The conditional request failed" then we send a code 200
            because the delete could success but the condition expression return false
            because the value of id not exist in the table*/
            return responseFunction.buildResponse(200,"This id doesn't exist");
        }
        else{
            /* if is not match with the message "The conditional request failed" then we send a code 404
            and the description of the error*/
            return responseFunction.buildResponse(404,error);
        }
        
    });
}
module.exports = { deleteStudent };