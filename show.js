// We import the aws sdk
const AWS = require("aws-sdk");
//Thanks to the aws sdk we call the DynamoDb Document Client
const dynamo = new AWS.DynamoDB.DocumentClient();
// We store the name of our table in constant
const dynamoTable = "student";

//We import the buildResponse function
const responseFunction = require("./response.js");
/*The function that will show the data of a student. 
We need to give the id of the student which send in parameter*/
async function getStudent(studentId) {
    // Then we give the parameters and inside we describe : 
    const params = {
        // The name of table
        TableName: dynamoTable,
        // The key of the item we want to show (here the key is the id of student)
        Key: {
            "id":studentId,
        }
    }
    /*Then we return a promise where the documentClient call the method get to show the data 
    of the student with parameters that we describe before*/
    return await dynamo.get(params).promise().then((response)=>{
        //We return buildResponse with the student data and the code 200
        return responseFunction.buildResponse(200,response.Item);
    }, (error) => {
        // if there is an error, we send a code 404 and the description of the error
        return responseFunction.buildResponse(404,error);
    });
}
// We export here the function to call it in the index.js
module.exports = { getStudent };