const AWS = require("aws-sdk");
const dynamo = new AWS.DynamoDB.DocumentClient();
const dynamoTable = "student";

//We import here each file for each event

//Import the file show for the GET method
const showFunction = require("./show.js");
//Import the file create for the POST method
const createFunction = require("./create.js");
//Import the file show for the PUT method
const updateFunction = require("./update.js");
//Import the file show for the DELETE method
const deleteFunction = require("./delete.js");

const searchFunction = require("./search.js");
//We define all paths that we use for our CRUD

// This is the show Path (GET)
const showPath = "/school/students/show";
//This is the delete path (DELETE)
const deletePath = "/school/students/delete";
//This is the the update path (PUT)
const updatePath = "/school/students/update";
//This is the create path (POST)
const createPath = "/school/students/create";



const searchPath = "/school/students/search"

//We import the buildResponse function
const responseFunction = require("./response.js");



exports.handler = async function(event){
    let response;
    switch(true){
        // Case if the event path is /school/students/show
        case event.path === showPath:
            // We will call the getStudent function from the file show.js
            response = await showFunction.getStudent(event.queryStringParameters.id);
            break;
        // Case if the event path is /school/students/create
        case event.path === createPath:
            // We will call the saveStudent function from the file create.js
            response = await createFunction.saveStudent(JSON.parse(event.body));
            break;
        // Case if the event path is /school/students/update
        case event.path === updatePath:
            // We will call the setStudent function from the file update.js
            response = await updateFunction.setStudent(JSON.parse(event.body),event.queryStringParameters.id);
            break;
        // Case if the event path is /school/students/delete
        case event.path === deletePath:
            // We will call the deleteStudent function from the file delete.js
            response = await deleteFunction.deleteStudent(event.queryStringParameters.id);
            break;
        // Case if the event path is /school/students/search
        case event.path === searchPath:
            // We will call the saveStudent function from the file search.js
            response = await searchFunction.searchStudent(JSON.parse(event.body));
            break;
        default:
            // if the event.path is not match with any previous case then we send an error message
            response = responseFunction.buildResponse(404, event.path);
            
    }
    return response;
}