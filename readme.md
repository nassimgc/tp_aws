# School API

This API contains a CRUD for students. You can add (add enpoint), delete (delete endpoint), search by id (show endpoint) or any field (search endpoint) and modify (update endpoint) a student.

The link of the api : https://2ka09g79ol.execute-api.eu-west-3.amazonaws.com/studentApi/school/students/

## Tools

To make this API I use AWS with dynamoDB service and I code with node.

## More information

To have more details on the features of this API, you can look the following link which contain the documentation and examples : 
https://documenter.getpostman.com/view/17809944/UUy7a4EE