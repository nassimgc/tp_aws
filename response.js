
/*function that will build a response which be sending when a method is called.
The parameters are a status code and the message (body)*/
function buildResponse(statusCode, body) {
    return {
        statusCode: statusCode,
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify(body)
    }
}
module.exports = { buildResponse};