// We import the aws sdk
const AWS = require("aws-sdk");
//Thanks to the aws sdk we call the DynamoDb Document Client
const dynamo = new AWS.DynamoDB.DocumentClient();
// We store the name of our table in constant
const dynamoTable = "student";

//We import the buildResponse function
const responseFunction = require("./response.js");
//The function that will save the student. We need to give the body of the JSON request
async function saveStudent(requestBody){
    // Then we give the parameters and inside we describe : 
    const params = {
        // The name of table
        TableName:dynamoTable,
        // The item that would be save
        Item: {
        "id": requestBody.id,
        "last_name": requestBody.last_name,
        "first_name": requestBody.first_name,
        "age": requestBody.age,
        "phone": requestBody.phone,
        "email": requestBody.email
        },
        //We check if the id exist or not in the table
        ConditionExpression: 'attribute_not_exists(id)',
    }
    /*Then we return a promise where the documentClient call the method 
    put to create the data with parameters that we describe before*/
    return await dynamo.put(params).promise().then(() =>{
        // We build the body of the message of success if the put method going well
        const body = {
            Operation: 'SAVE',
            Message:'SUCCESS',
            Item: requestBody
        }
        //We return buildResponse with the message of success and the code 200
        return responseFunction.buildResponse(200,body);
    }, (error)=>{
        // We check if there is an error
        if(error.message == "The conditional request failed"){
            /* If the error have the message "The conditional request failed" then we send a code 200
            because the put could success but the condition expression return false
            because the value of id already exist in the table*/
            return responseFunction.buildResponse(400,"The username is already used");
        }else if(error.message == "One or more parameter values were invalid: Missing the key id in the item"){
            return responseFunction.buildResponse(400,"You have to give an id. It could be a username or a number");
        }
        else{
            /* if is not match with the message "The conditional request failed" then we send a code 404
            and the description of the error*/
            return responseFunction.buildResponse(404,error);
        }
    })
    
}

// We export here the function to call it in the index.js
module.exports = { saveStudent };