// We import the aws sdk
const AWS = require("aws-sdk");
//Thanks to the aws sdk we call the DynamoDb Document Client
const dynamo = new AWS.DynamoDB.DocumentClient();
// We store the name of our table in constant
const dynamoTable = "student";

//We import the buildResponse function
const responseFunction = require("./response.js");

/*The function that will update the data of a student. We need to give the id of 
the student which send in parameter and the body of the JSON request */
async function setStudent(requestBody, studentId) {

    /* We call the dynamicUpdateQuery to prepare the update
    expressions needed for the parameters. We will give
    the requestBody as parameter of the function*/
    let expression = dynamicUpdateQuery(requestBody);
     // Then we give the parameters and inside we describe : 
    let params = {
        // The name of table
        TableName: dynamoTable,
        // The key of the item we want to update (here the key is the id of student)
        Key: {
            "id":studentId,
        },
        // The item that would be save
        Item:{
            "last_name": requestBody.last_name,
            "first_name": requestBody.first_name,
            "age": requestBody.age,
            "phone": requestBody.phone
        },
        //We check if the id exist or not in the table
        ConditionExpression: 'attribute_exists(id)',
        /* We use the spread operator to spread the object that 
           we retrieve from the dynamicUpdateQuery function*/
        ...expression,
        //Returns all of the attributes of the item, as they appeared before the Update operation.
        ReturnValue: "ALL_OLD"
    };
    /*Then we return a promise where the documentClient call the method update to update the data with parameters
     that we describe before*/
    return await dynamo.update(params).promise().then(() =>{
        // We build the body of the message of success if the put method going well
        const body = {
            Operation: 'UPDATE',
            Message:'SUCCESS',
            Item: requestBody
        }
        //We return buildResponse with the message of success and the code 200
        return responseFunction.buildResponse(200,body);
    }, (error)=>{
        // We check if there is an error
        if(error.message == "The conditional request failed"){
            /* If the error have the message "The conditional request failed" then we send a code 200
            because the put could success but the condition expression return false
            because the value of id not exist in the table*/
            return responseFunction.buildResponse(200,"This email doesn't exist");
        } else if (error.message == "ExpressionAttributeNames must not be empty"){
            return responseFunction.buildResponse(200,"You need to give at least one valable field.");
        }else{
            /* if is not match with the message "The conditional request failed" then we send a code 404
            and the description of the error*/
            return responseFunction.buildResponse(404,error);
        }
    })
}


/*function that will add dynamically the value inside the update expression
according to the JSON body request in parameters*/
function dynamicUpdateQuery(requestBody)
{
    /*
        We define here an object which contain all expressions
        that we need to make our search in a dynamic way
    */
    const listItemAvailable = ["last_name","first_name","age","email","phone"];
    let expressionObject = {
        UpdateExpression: 'set',
        ExpressionAttributeNames: {},
        ExpressionAttributeValues: {}
    }
    
    /*
        We iterate the requestBody to fill the expressions
        inside of the object
    */
    Object.entries(requestBody).forEach(([key, item]) => {
        /*We fill the filter expression. We need it to use the scan method
        inside the searchStudent function. 
        eg : FilterExpression : #nom = :nom AND #prenom = :prenom
        */
        expressionObject.UpdateExpression += ` #${key} = :${key},`;
        //We declare the attributes names that we use in our filter expression
        if(key != "id" && listItemAvailable.includes(key)){
           expressionObject.ExpressionAttributeNames[`#${key}`] = key; 
        }
        //We declare the attributes values that we use in our filter expression
        expressionObject.ExpressionAttributeValues[`:${key}`] = item;
    });
    
    /*  We use here a slice on the filter expression
        to remove the last "coma to avoid an error */
    expressionObject.UpdateExpression = expressionObject.UpdateExpression.slice(0, -1);
    
    //We return the object that we build
    return expressionObject;
    
}


// We export here the function to call it in the index.js
module.exports = { setStudent };